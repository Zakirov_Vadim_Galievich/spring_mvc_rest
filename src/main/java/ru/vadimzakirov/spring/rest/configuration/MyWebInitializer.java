package ru.vadimzakirov.spring.rest.configuration;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class MyWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return null;
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{MyConfig.class}; //добавили конфигурационный файл
    }


//    Метод ниже эквивалентен записи:
//    <servlet-mapping>
//    <servlet-name>dispatcher</servlet-name>
//    <url-pattern>/</url-pattern>
//  </servlet-mapping>
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
